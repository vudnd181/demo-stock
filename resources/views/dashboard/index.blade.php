@extends('layout')
@section('title')
  {{ $title }}
@endsection
@section('content')
<section class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{$title}}
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="/"><i class="fa fa-dashboard"></i>Dashboard</a></li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-body">
        <div class="col-xl-4">
          <div class="card" style="padding:0 10px;">
              <div class="card-body">
                <div class="text-right text-muted"></div>
                  
                  <h4 class="mt-0">Các cổ phiếu còn lại sau mua bán</h4>
                  @foreach($data->remain as $item)
                    <div class="card mb-3 border-primary col-lg-12" style="border:1px solid gray">
                      <div class="card-body">
                          <h4 class="card-title text-primary">Cổ phiếu {{ $item->code }}: {{ $item->remain_number }}</h4>
                          @if($item->logs) 
                            @if(isset($item->logs->max))
                            <p class="card-text">
                              Thời gian nắm giữ dài nhất {{ $item->logs->max }} ngày 
                            </p>
                            @endif
                            @if(isset($item->logs->min))
                              <p class="card-text">
                                Thời gian nắm giữ ngắn nhất {{ $item->logs->min }} ngày 
                              </p>
                            @endif
                          @endif
                      </div>
                    </div>
                  @endforeach
              </div>
          </div>
        </div>
      </div>
    </div>
    
  </section>  
</div>
@endsection
@section('extends_js')
@endsection