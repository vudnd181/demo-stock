<aside class="main-sidebar">
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="">
        <a href="/">
          <i class="fa fa-files-o"></i> <span>Dashboard</span>
        </a>
      </li>
      <li class="">
        <a href="/stock">
          <i class="fa fa-files-o"></i>
          <span>Quản lý Stock</span>
        </a>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
