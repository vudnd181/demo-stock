@extends('layout')
@section('title')
  {{ $title  }}
@endsection
@section('extends_css')
<link rel="stylesheet" href="/admin/plugin/bootstrap-datepicker/bootstrap-datepicker.css">
@endsection
@section('content')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      {{$title}}  
      
      <small> {{ isset($data->id) ? "Edit" : "Add New" }} </small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> {{$title}}</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <div class="row">
          <div class="col-xs-6">
          </div>
          <div class="col-xs-6 text-right">
            <a href="/stock" class="btn btn-success btn-flat">
              <i class="fa fa-arrow-up"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Back to list</b>
            </a>
          </div>
        </div>
      </div>
      <section>
        <div class="box">
          <form class="form-horizontal" action="{{ url()->full() }}" method="POST" name="form" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="box-body">
              @if( $messageBox->message !="")
              <div class="box-error callout callout-{{ $messageBox->status }}">{{ $messageBox->message }}</div>
              @else
              <div class="box-error callout callout-danger" style="display: none;"></div>
              @endif
              <div class="form-group">
                <label class="col-sm-2 control-label">ID:</label>
                <div class="col-sm-10">
                  @if(isset($data->id))
                  <input type="text" name="id" class="form-control" placeholder="" readonly="readonly" value="{{ $data->id }}"/>
                  @else
                  <input type="text" name="id" class="form-control" placeholder="" readonly="readonly"
                          value="Generate new identify"/>
                  @endif
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Type:</label>
                <div class="col-sm-10">
                  @if(isset($data->type))
                  <select name="type" class="form-control">
                    <option value="SELL" {{ $data->type == "SELL" ? "selected":""}}>SELL</option>
                    <option value="BUY" {{ $data->type == "BUY" ? "selected":""}}>BUY</option>
                  </select>
                  @else
                  <select name="type" class="form-control">
                    <option value="BUY">BUY</option>
                    <option value="SELL">SELL</option>
                  </select>
                  @endif
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Code:</label>
                <div class="col-sm-10">
                  @if(isset($data->code))
                  <input type="text" name="code" class="form-control" placeholder="" value="{{ $data->code }}" required />
                  @else
                  <input type="text" name="code" class="form-control" placeholder="" value="" required/>
                  @endif
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Quantity:</label>
                <div class="col-sm-10">
                  @if(isset($data->quantity))
                  <input type="number" name="quantity" class="form-control" placeholder="" value="{{ $data->quantity }}" required/>
                  @else
                  <input type="number" name="quantity" class="form-control" placeholder="" value="" required/>
                  @endif
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Date:</label>
                <div class="col-sm-10">
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    @if(isset($data->date))
                    <input type="text" class="form-control pull-right" id="datepicker" name="date" value="{{ $data->date }}" data-date-format="yyyy-mm-dd" required>
                    @else
                    <input type="text" class="form-control pull-right" id="datepicker" name="date" placeholder="" value="" data-date-format="yyyy-mm-dd" required/>
                    @endif
                  </div>
                </div>  
              </div>
              <div class="box-footer">
                <a href="javascript:location.reload();" class="btn btn-default btn-flat">Reload</a>
                <button type="submit" class="btn btn-info btn-flat pull-right">Save</a>
              </div>
            </div>
          </form>
        </div>
      </section>
    </div>
  <section>
</div>
@endsection
@section('extends_js')
<script src="/admin/plugin/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script>
  $(document).ready(function() {
    $('#datepicker').datepicker({
      autoclose: true,
      dateFormat: 'yy-mm-dd'
    });
  })
</script>
@endsection

