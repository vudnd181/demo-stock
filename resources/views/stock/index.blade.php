@extends('layout')
@section('title')
  {{ $title }}
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{$title}}
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="/"><i class="fa fa-dashboard"></i>Dashboard</a></li>
      <li class="active">{{ $title }}</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <div class="row">
          <div class="col-xs-6">
          </div>
          <div class="col-xs-6 text-right">
            <a href="{{ url()->full() }}/modify" class="btn btn-success btn-flat"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Add new</b></a>
          </div>
        </div>
      </div>
      @if($data)
      <div class="box-body">
        <div class="col-xs-12 table-responsive">
          <table class="table table-hover">
            <tbody>
            <tr>
              <th width="5%">ID</th>
              <th width="">Code</th>
              <th width="">Quantity</th>
              <th width="">Type</th>
              <th width="">Date</th>
              <th width="10%" class="text-right">Management</th>
            </tr>
            @foreach($data as $item)
            <tr data-id="{{ $item->id }}">
              <td>{{$item->id}}</td>
              <td>{{$item->code}}</td>
              <td>{{$item->quantity}}</td>
              <td>{{$item->type}}</td>
              <td>{{$item->date}}</td>
              <td class="text-right">
                <a href="/stock/modify?id={{$item->id}}" class="btn btn-warning btn-flat">
                  <i class="fa fa-edit"></i>
                </a>
                <a href="javascript:;" class="btn-delete btn btn-danger btn-flat">
                  <i class="fa fa-close"></i>
                </a>
              </td>
            </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
      @if($data->lastPage() > 1)
      <div class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-right">
        @for($i = 1 ;$i<= $data->lastPage(); $i++)
          <li class=""><a href="{{ url()->current() }}?page={{$i}}">{{$i}}</a></li>
        @endfor
        </ul>
      </div>
      @endif
      @endif
    </div>  
  </section>
  <!-- Main content -->
  <!-- /.content -->
</div>
@endsection
@section('extends_js')
<script type="text/javascript">
$(function() {
  if($('.btn-delete').length >0){
    $('.btn-delete').on('click',function(){
      var id = $(this).parents('tr').data('id');
      swal({
        title: "Xoá Stock này ?",
        text: 'Xoá Stock Lưu ý: Bạn sẽ không khôi phục lại được',
        type:"warning",
        showReloadButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes, xoá",
        showCancelButton: true,
        closeOnConfirm: false
      }, function() {
        $.ajax({
          url: `/stock/delete?id=${id}`,
          method: 'POST',
          headers: {
            'X-CSRF-TOKEN': '{!! csrf_token() !!}'
          },
          success: function(data){
            if(data) {
              swal({
                title: 'Deleted!',
                text:'',
                type:'success'
              },function(){
                window.location.href = window.location.href;
              });
            }
          },
          error: function(err) {
            swal('Lỗi', err,'error');
          }
        })
      })
    });
  }
})
</script>
@endsection