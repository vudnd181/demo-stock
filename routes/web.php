<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index');
Route::get('/stock', 'StockController@index');
Route::match(['get', 'post'], '/stock/modify', 'StockController@modify');
Route::post('/stock/delete', 'StockController@delete');
Route::get('/stock/test', 'StockController@test');

Route::get('/stock/test-logs', 'StockController@testLogs');
