<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class Basecontroller extends Controller
{
  protected $messageBox;
  protected $current;

  public function __construct(){
    $this->messageBox = new \stdClass;
    $this->messageBox->message ="";
    $this->messageBox->status ="";
    $this->current =  Carbon::now(+7);
  }
}