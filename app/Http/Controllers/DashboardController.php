<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Helpers\RecordHelper;

class DashboardController extends BaseController {
  public function __construct(){
    parent::__construct();
  }

  public function index(Request $request) {
    $title = 'Dashboard';
    $data = RecordHelper::I()->Dashboard();
    return view('dashboard.index',
      compact('title', 'data')
    );
  }
}