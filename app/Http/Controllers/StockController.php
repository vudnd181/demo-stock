<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Models\Stock;
use App\Helpers\RecordHelper;

class StockController extends BaseController {
  public function __construct(){
    parent::__construct();
  }

  public function index(Request $request) {
    $title = 'Quản lý Stock';
    $data = Stock::where('status', 1)->paginate(15);
    return view('stock.index',
      compact('title', 'data')
    );
  }

  public function modify(Request $request) {
    $title = 'Chi tiết Stock';
    $messageBox = $this->messageBox;
    $id = $request->input('id');
    $data = Stock::where('id', $id)->first();
    if ($request->method() == 'GET') {
      return view('stock.modify',
        compact('title', 'messageBox', 'data')
      );
    }
    if ($request->method() == 'POST') {
      \DB::beginTransaction();
      $type = $request->type;
      
      if($id == 0) {
        // Create
        $data = new Stock();
        $data->type = $type;
        $data->code = $request->code;
        $data->date = $request->date;
        $data->quantity = $request->quantity;

        $status = $data->save();
        if ($type == 'BUY') {
          $record = RecordHelper::I()->makeBuyRecord($data);
        } else {
          $record = RecordHelper::I()->makeSellRecord($data);
        }
        unset($data->id);
      } else {
        // Update
        $data->quantity = $request->quantity;
        $data->date = $request->date;
        $data->type = $type;
        $record = RecordHelper::I()->generateLogsEdit($data);
        \DB::commit();
      }
      if($record->status) {
        \DB::commit();
        $this->messageBox->message="TẠO THÀNH CÔNG";
        $this->messageBox->status="success";
      } else {
        \DB::rollback();
        $this->messageBox->message="THẤT BẠI - Không hợp lệ";
        $this->messageBox->status="danger";
      }
      return view('stock.modify',
        compact('title', 'messageBox', 'data')
      );

    }
  }

  public function delete(Request $request) {
    $id = $request->id;
    $result = RecordHelper::I()->generateLogsDelele($id);
    return response()->json([
      'data'=> $result
    ]);
  }

  public function testLogs(Request $request) {
    $test = RecordHelper::I()->makeRecordLog();
  }
}