<?php

namespace App\Helpers;
use Illuminate\Http\Request;

class BaseHelper {
  private static $inst = null;

  public static function I() {
    $c = __CLASS__;
    if(self::$inst === null) self::$inst = new $c();
    return self::$inst;
  }
  protected function back($error = 0, $data= [], $message = '') {
    $o = new \stdClass();
    $status = true;
    if($error != 0)
      $status = false;
    $o->status = $status;
    $o->data = $data;
    if($message != '')
      $o->message = $message;
    return $o;
  }

  protected function backSuccess($data = [], $message = '') {
    return $this->back(0, $data, $message);
  }

  protected function backException($message = "", $data = []) {
    return $this->back(1, $data, $message);
  }
}
