<?php
namespace App\Helpers;

use App\Helpers\BaseHelper;
use App\Models\Stock;
use App\Models\RecordLogs;

class RecordHelper extends BaseHelper {
  private static $inst = null;

  public static function I() {
    $c = __CLASS__;
    if (self::$inst === null) self::$inst = new $c();
    return self::$inst;
  }

  public function getDays($buyDate, $sellDate) {
    $buyDateObj = date_create($buyDate);
    $sellDateObj = date_create($sellDate);
    $diff = date_diff($buyDateObj, $sellDateObj);
    return $diff->format("%a");
  }

  public function makeBuyRecord($stock) {
    \DB::beginTransaction();
    try {
      $record = new RecordLogs([
        'buy_id' => $stock->id,
        'remain_quantity' => $stock->quantity,
      ]);
      $record->save();
      \DB::commit(); 
      return $this->backSuccess($stock); 
    } catch(\Exception $e) {
      dd($e);
      \DB::rollback();
      return $this->backException($e->getMessage());
    }
  }

  public function makeSellRecord($stock) {
    $sell_quantity = $stock->quantity;
    \DB::beginTransaction();
    try {
      while ($sell_quantity > 0) {
        $record = \DB::table('record_logs')->where('code', $stock->code)
                              ->join('stocks','record_logs.buy_id', 'stocks.id')
                              ->where('record_logs.remain_quantity', '>',  0)->orderBy('stocks.date', 'asc')
                              ->select('record_logs.id as id', 'remain_quantity', 'logs', 'date')
                              ->first();
        $record_update = RecordLogs::where('id', $record->id)->first();
        $days = $this->getDays($record->date, $stock->date);
        $logs = $record_update->logs;
        if (isset($logs)) {
          $metadata = json_decode($logs);
          array_push($metadata, [
            $stock->id => (int)$days,
          ]);
          $record_update->logs = json_encode($metadata);
        } else {
          $meta[] = [
            $stock->id => (int)$days,
          ];
          $record_update->logs = json_encode($meta);
          $meta = [];
        }
        if ($sell_quantity > $record->remain_quantity) {
          $remain = $sell_quantity - $record->remain_quantity;
          $record_update->remain_quantity = 0;
          $record_update->save();
          $sell_quantity = $remain;
          continue;
        } else {
          $record_update->remain_quantity = $record->remain_quantity - $sell_quantity;
          $record_update->save();
          break;
        }
      }
      \DB::commit();
      return $this->backSuccess($stock);
    } catch(\Exception $e) {
      \DB::rollback();
      return $this->backException($e->getMessage());
    } 
  }

  public function generateLogsDelele($id) {
    \DB::beginTransaction();
    try {
      $stock = Stock::where('id', $id)->first();
      $code = $stock->code;
      if ($stock->type == 'BUY') {
        $record = RecordLogs::where('buy_id', $id)->first();
        $record->delete();
      }
      $stock->delete();
      $this->processRecordByCode($code);
      \DB::commit();
      return $this->backSuccess();

    } catch(\Exception $e) {
      \DB::rollback();
      return $this->backException($e->getMessage());
    }
    
  }

  public function generateLogsEdit($data) {
    \DB::beginTransaction();
    try {
      Stock::where('id', $data->id)->update([
        'quantity' => $data->quantity,
        'date' => $data->date,
      ]);
      $this->processRecordByCode($data->code);
      \DB::commit();
      return $this->backSuccess();
    } catch(\Exception $e) {
      \DB::rollback();
      return $this->backException($e->getMessage());
    }
  }

  public function processRecordByCode($code) {
    $record = \DB::table('record_logs')->where('code', $code)
      ->join('stocks','record_logs.buy_id', 'stocks.id')
      ->select('record_logs.id as id', 'remain_quantity', 'logs', 'date', 'stocks.quantity')
      ->get();
    foreach ($record as $item) {
      RecordLogs::where('id', $item->id)->update([
        'remain_quantity' => $item->quantity,
        'logs' => null,
      ]);
    }
    // Get Sell Item for generate 
    $stockSell = Stock::where('code', $code)->where('type', 'SELL')->get();
    foreach($stockSell as $item) {
      $result = $this->makeSellRecord($item);
    }
  }

  public function Dashboard() {
    $data = new \stdClass();
    $remain = [];
    $code = Stock::select()->groupBy('code')->get();
    foreach($code as $item) {
      $remainItem = new \stdClass();
      $record = RecordLogs::join('stocks','record_logs.buy_id', 'stocks.id')
        ->where('code', $item->code)
        ->select('record_logs.logs', 'record_logs.remain_quantity')
        ->get();
      $remain_number = 0;
      $logs = [];
      foreach($record as $index => $i) {
        $remain_number += $i->remain_quantity;
        if (isset($i->logs)){
          if ($index == 0) {
            $logs = json_decode($i->logs);
          } else {
            $logs = array_merge($logs, json_decode($i->logs));
          }
        }
      }
      $remainItem->remain_number = $remain_number;
      $remainItem->code = $item->code;
      
      $remainItem->logs = $this->getMaxAndMin($logs);
      $remain[] = $remainItem;
    }
    $data->remain = $remain;
    return $data;
  }

  public function getMaxAndMin($list){ 
    try {
      $lstItem = [];
      $data = new \stdClass();
      if(is_array($list)) {
        foreach($list as $index => $item) {
          foreach($item as $key => $value) {
            $lstItem[] = $item->$key;
          }
        }
        if(isset($lstItem) && count($lstItem) > 0) {
          $data->min = min($lstItem) ?? '';
          $data->max = max($lstItem) ?? '';
        }
      }
      return $data;
    } catch (\Exception $e) {
      return $e->getMessage();
    }
    
  } 
}