<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $fillable = ['code', 'quantity', 'date', 'type'];

    protected static function boot()
    {
        parent::boot();
    
        static::creating(function ($query) {
            $query->status = 1;
        });
    }
}
