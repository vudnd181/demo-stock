<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecordLogs extends Model
{
    protected $fillable = ['remain_quantity', 'buy_id'];
}
