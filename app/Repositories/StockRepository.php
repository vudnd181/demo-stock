<?php

namespace App\Repositories;
use App\Models\Stock;

class StockRepository{
  protected $model;

  public function __construct() {
    $this->model = $this->model = app()->make(Stock::class);
  }

  public function storeStock($data) {
    $stock = $this->model->create($data);
    return $stock;
  }
}

