<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TestStock extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testUserViewStock()
    {
        $response = $this->get('/stock123');
        $response->assertStatus(200);
    }

    protected $stock;

    public function setUp()
    {
        parent::setUp();
        $this->faker = Faker::create();
        $this->stock = [
            'name' => $this->faker->name,
            'description' => $this->faker->name,
        ];
        $this->stockRepository = new StockRepository();
    }

    public function testStore() {
        $stock = $this->stockRepository->storeStock($this->stock);
        $this->assertInstanceOf(Stock::class, $stock);
    }

    public function tearDown()
    {
        parent::tearDown();
    }
}
